\contentsline {chapter}{\numberline {1}Introduction}{1}{chapter.1}
\contentsline {chapter}{\numberline {2}Neutrino Telescopes}{3}{chapter.2}
\contentsline {chapter}{\numberline {3}Experimental setup - Hodoscope}{5}{chapter.3}
\contentsline {chapter}{\numberline {4}Antares OM Study}{7}{chapter.4}
\contentsline {chapter}{\numberline {5}KM3NeT DOM Characterization}{9}{chapter.5}
\contentsline {chapter}{\numberline {6}ANTARES in-situ OM studies}{11}{chapter.6}
\contentsline {chapter}{\numberline {7}KM3NeT in-situ DOM studies}{13}{chapter.7}
\contentsline {chapter}{\numberline {8}Conclusions}{15}{chapter.8}
\contentsline {chapter}{\numberline {A}Appendix: Hodoscope}{17}{appendix.A}
\contentsline {subsection}{\numberline {A.0.1}Muon Track Determination}{17}{subsection.A.0.1}
\contentsline {subsubsection}{\numberline {A.0.1.1}Geometry corrections on timing}{17}{subsubsection.A.0.1.1}
\contentsline {subsection}{\numberline {A.0.2}Track-Signal Results}{17}{subsection.A.0.2}
\contentsline {subsection}{\numberline {A.0.3}Angular Acceptance}{17}{subsection.A.0.3}
\contentsline {chapter}{Bibliography}{19}{appendix*.5}
