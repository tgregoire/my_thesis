# Outline

## Neutrino astronomy
### General context
    Astro nu history (past)
    Astro nu state of the art (present)
    Multimessenger astronomy (future)

### Diffuse Galactic emission
    CR propagation
    KRAγ model…

### Gravitational waves
    Relat G
    Binary neutron stars
    Neutrino flux from binary neutron stars

## The experiment
    ANTARES detector
    Simulation
    Outlook of KM3NeT detector

## Galactic Plane analysis
### ANTARES alone
    Data set
    Event selection
    Search method
    Results
    
### Combination with IceCube
    Search method
        Bias correction
        Combination
    Results

## Analyse GWHEN 170817
    Context, multimess
    Data set
    Event selection
    Search method
    Results
    
# Planning
Mars: Galactic Plane analysis
Avril: Analyse GWHEN 170817
Mai: The experiment + Diffuse Galactic emission
Juin: General context + Gravitational waves

Suggestion:
Mettre théorie Gravitational waves et diffuse emission at the begining of the dedicated chapter
