Limit on (6yrs) HESE for combination

☐ Get pourcentage de HESE avec HESE six years
    ☐ 82 events
        ☐ Dont 39.59 expected from background in total (avec un bin ou ils en ont 10 de moins qu'attendu)
    ☐ Faire un mail à Antonio avec les infos: 
        For what concerns the limit on the number of HESE, it is 15%.
        Details below: 
        * limit of 6.271 HESE with the UL at 0.90 on 50PeV
            * Live time: 2078 days for 6 years
        * 47 HESE above 60 TeV
            * with 6.003 expected from background (above 60 TeV)
        * 6.27/(47-6.00) = 15.29%
        It was 18% in the previous paper.

puis le code limit_on_HESE.py donne le nombre d'évts attendu dans le nord et sud, donc on calcule le nombre d'evts qui peuvent participer à l'asymmetrie N/S
