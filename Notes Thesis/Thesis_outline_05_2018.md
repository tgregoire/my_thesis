# Outline

## I. Neutrino astronomy
### 1) General context
    Astro nu history (past)
    Astro nu state of the art (present)
    Multimessenger astronomy (future)

### 2) The ANTARES experiment
    ANTARES detector
        Background description (atmo muons, K40, biolum)
        Data quality
        Track and shower reconstructions
            Et au passage parler des quality cuts, qu'on étend en dehors de trucs liés aux reco
        Parler du Monte Carlo et de la simu du bg

### 3) Other Neutrino experiments
    Outlook of KM3NeT detector
    IceCube detector (for combination)
    Auger

## II. Galactic Plane analysis
Hulse taylor detection

### 1) Diffuse Galactic emission
    CR propagation
    KRAγ model…

### 2) Data set
    Data set
    Run selection
    Event selection
    
### 3) Search method
    A bit of math: Demonstration of the likelihood
    The analysis itself
    
### 4) Results
    
## III. Analyse GWHEN 170817
### 1) Astronomy gravitational wave
#### a) Theory
    Relat G, Gravitationnal waves (Quickly)
    Astronomical sources: BH-BH, NS-NS, NS-BH (Quickly)
    Binary NS models: Neutrino flux from NS-NS

#### b) Detectors
    (mesurer variation de la taille de l'épaisseur d'un cheveux sur une distance de ici a alpha du centaure)
    Advanced LIGO, advanced Virgo (Quickly)
        How does it work (interferometer)
        What expected signal look like (tchirp)
        Expliquer fit des data avec template

### 2) GW170817
    Context
    Event characteristics
    Chronology of the detection
    Physical implications

### 3) Neutrino follow-up GW170817
    Search method
    Results
    
# Planning
Mars: Galactic Plane analysis jusqu'au 15 avril
Avril: Analyse GWHEN 170817
Mai: The experiment + Diffuse Galactic emission
Juin:  Gravitational waves + General context
