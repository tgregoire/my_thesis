\sim290 TeV  (with a 90\%C.L. lower limit of 183 TeV) neutrino event spatially correlated with a previously identified Fermi-LAT-catalogued blazar TXS 0506+056
signalness of \sim56\% accounting for energy and zenith (comment c'est estimé? est-ce que ça suppose un flux de signal?)
Source was flaring since months (vu par Fermi et MAGIC)
Obs from radio to VHE-γ
3σ rejection de bg:
    ie rejection at 3σ d'avoir un événements suffisement bon pour être une alerte (10+41 trials) (avec cette énergie et zenith là je suppose) corrélée avec un *blazar* ayant un tel γ-ray flux qui soit du bg
    Perso j'aurais aimé connaitre le niveau de rejection d'avoir un tel événement corrélé avec une source avec un flux mesuré en γ-ray aussi important que ce blazar (aussi forte ou moins forte mais plus proche)
        Ça donne potentiellement un trucen dessous de 3σ sauf si il n'y a pas de truc autre que blazar aussi fort en γ que ce blazar
    Par contre, la probabilité que cet événements soit extragalactique n'est pas prise en compte, je suppose, or dans les 56\% d'être du signal il y a une chance non négligeable d'être extragal. Il faudrait calculer la probabilité qu'un événements extragal soit corrélé avec une source.
        cette dernière remarque est nulle et non-avenue si ils ne prennent pas en compte la signalness dans leur lik, dans quel cas ils évaluent juste la proba qu'un truc distribué isotropiquement (extragal ou bg) tombe sur un blazar

Ils ont ensuite regardé en arrière dans les data
entre sept 2014 et march 2015 ils voient un excès de 13±5 over bg
3.5σ
mais pas corrélé à plus de γ par contre les γ semblent avoir plus hautes énergies

Possibility que le blazar d'à coté PKS 0502+049 soit la source du signal 2014/15 est exclut (voir comment, est-ce qu'il est également exclut qu'une partie du signal vienne d'ici et une autre de là??)

Antares blabla

Proof que blazar sont une source de VHE CR acceleration
Blazar jets could accelerate CR to at least several PeV

Question
Donc le signal γ n'est pas transient?
    Nop, ils envoient une alerte et les gens voient quelque chose en optique
    C'est équivalent à une recherche point source en mode catalogue mais online
Je veux savoir la résolution angulaire du truc
    1.6° au max 90\% → 0.97 deg² (2.4·10⁻³\% du ciel)
Combien y a t il d'événements avec cette énergie (estimée) ou plus?
    j'en compte 18 à partir de figure 4 de OBSERVATION AND CHARACTERIZATION OF A COSMIC MUON NEUTRINO FLUX FROM THE NORTHERN HEMISPHERE USING SIX YEARS OF ICECUBE DATA
    Dans toutes les données IceCube il y a 41 evts qui auraient donné une alerte si le système était prêt (ça me convient parfaitement pour évaluer l'équivalent et c'est ce qu'ils ont fait)
