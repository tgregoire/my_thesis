## Background:
muon rate (atmospheric dominating): O(100) Hz/km
40K decays (~constant): O(10) kHz/PMT (3”, 0.5 p.e. thld) 
Bioluminescence(occasional): O(100) kHz/PMT (3”, 0.5 p.e. thld)

Fibre: 
80 colours / fibre (spaced bye 50 MHz) are used
With 36 fibres


The probability of a Cherenkov photon at k = 470 nm to
suffer from light scattering on a path of 60 m (the inter-line dis-
tance and coincidentally the water absorption length) is only few
per cent [13].

