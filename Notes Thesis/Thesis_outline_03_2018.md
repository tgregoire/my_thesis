# Outline

## I. Neutrino astronomy
### 1) General context
    Astro nu history (past)
    Astro nu state of the art (present)
    Multimessenger astronomy (future)

### 2) The experiment
    ANTARES detector
    Outlook of KM3NeT detector
    IceCube detector (for combination)

## II. Galactic Plane analysis
### 1) Diffuse Galactic emission
    CR propagation
    KRAγ model…

### 2) Data set
    Data set
    Run selection
    Event selection
    
### 3) Search method
    A bit of math: Demonstration of the likelihood
    The analysis itself
    
### 4) Results
    
## III. Analyse GWHEN 170817
### 1) Gravitational waves
    Relat G
    Binary neutron stars
    Neutrino flux from binary neutron stars

### 2) Analyse GWHEN 170817
    Context, multimess
    Data set
    Event selection
    Search method
    Results
    
# Planning
Mars: Galactic Plane analysis
Avril: Analyse GWHEN 170817
Mai: The experiment + Diffuse Galactic emission
Juin: General context + Gravitational waves
