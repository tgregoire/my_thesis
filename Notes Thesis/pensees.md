Voir idées dans cahier

Moins écrire "as can be seen in figure…" mais plus "(figure…)"

conseil d'écriture, pour que ce ne soit pas trop lourd, éviter de mettre trop de mots de liaisons inutiles comme "indeed" etc…

Se faire un avis sur les modéles, bien décrire ça, les avantages et inconvénients de chacun, se faire un avis

Pourquoi cette analyse est utile (même à la société ?). Idem en moins long sur l'astro neutrino.

C'est normal que la médiane de la distrib 2D nsig tr+sh commence au dessus de 0 car on somme deux trucs ≥0

Faire un petit speech sur la collaboration en science puisque combinaison et multimessager (mais sans dire trop de banalités non plus)

Justifier mathématiquement autant que possible les fonctions que j'utilise pour les fits parce que c'est cool !

Je pourrais mettre quelque part dans un prelude que j'ai voulu éviter le jargon parce que ça n'apporte rien et que ça rend la science moins accessible (aux nouveaux venus)

Éventuellement ajouter image du slide 14/32 que Thierry a envoyé le 3 juillet pour le meeting grossmann

Point qu'il pourrait être intéressant de discuter:
Soudainement je me dis que c'est pas une très bonne idée que dans mon likelihood le bg ne soit fait qu'à partir de data, finalement, on a moins de signal que IceCube mais aussi moins de bg; sur cette analyse combinée en particulier, on a un meilleur rapport signal sur bruit que IceCube est-ce ce qui compte…
En effet on aurait peut-être que quelques éléments de signal mais ce qu'on fit est aussi quelques événements de signal. Bien sûr tout cela n'est pas une erreur, c'est juste une diminution de la sensibilité.
D'ailleurs peut-être que c'est vraiment négligeable et dans ce cas là la question est différente, est-ce censé de la part d'IceCube d'avoir fait leur likelihood tel qu'ils l'ont fait.

Pour évaluer la vraisemblance d'un modèle (c'est un peu vite dit dans le sens où ça manque de nuances) : Un modèle qui fit (bien) des données qui ont d'importantes barres d'erreurs en laissant des paramêtres libres de manière peu justifiée est moins vraisemblable qu'un modèle qui fit moins bien les données mais avec moins de paramêtres libres. En effet rajouter des paramêtres libres **ad hoc** juste parce qu'on sait qu'ils nous permettront de mieux fitter les données permettrait de fitter n'importe quoi donc même des fluctuations statistiques ou erreurs systematiques...
Bien sûr si un modèle fait des prédictions testables ça lui rajoute de l'intérêt (mais pas de la vraisemblance).

Lire comment KRAγ est présenté dans les papiers qui le cite.
Lire critiques de KRAγ dans articles ou PubView si existe.

Idée qui n'a pas grand chose à voir avec la thèse c'est que choisir d'optimiser pour chaque analyse nous fait perdre du temps, non seulement pour faire l'optimisation elle même mais aussi pour produire toutes les valeurs du genre energy range, résolution… Tout ça pour pas grand chose à mon avis…

Une question que je me pose, est-ce que le fait que les modèles conventionnels comme π₀-decay template ne reproduisent pas parfaitement les données tel qu'on le voit est attendu de leur simplifications (c'est souvent 2D...) et est-ce qu'il serait possible et quelque peu vraisemblable qu'on finisse par reproduire les données en gardant le même modèle mais en rafinant les hypothèses, et en ayant des ordinateurs plus puissants? Versus faire de nouvelles hypothèses.

Idée d'amélioration de l'analyse (en plus de mettre dans le lik un truc qui prend en compte toutes les valeurs sur lesquelles on coupe d'habitude): Faire une PDF qui dépend de la résolution angulaire (ne pas moyenner sur les résos comme on fait) comme ce que fait IceCube. Ce serait utile pour les showers en particulier. Néanmoins c'est plus utile pour IceCube que pour nous.

Dans mon lik je ne prends pas expressement en compte (comme bg) un potentiel signal autre que KRAγ. Néanmoins un signal isotrope, est compris dans mon background (il y ressemble en tout point) et de toute façon comme je met une UL, je suis juste plus conservatif.

Relire l'intro de la thèse de monvoisin qui était inspirante

Je pourrais écrire quelque part un truc sur la génialitude de l'utilisation d'un Monte Carlo (un genre de truc comme ce que je dirais à un non physicien pour lui expliquer le pouvoir de cette technique): si on a un biais mais pareil dans Monte Carlo, c'est pas grave; je sais que j'ai eu des trucs à dire dessus, mais aujourd'hui je ne vois plus quoi :-D
Quand j'étais jeune, je pensais que les scientifiques devaient être capable de simuler tout les trajets de chaque particules, etc pour pouvoir dire quelque chose alors qu'il suffit de faire tout statistiquement, d'utiliser des infos qui simplifient grandement le problème (background est isotrope...)
Bien sûr on a jamais totalement confiance en notre MC

ANTARES footprint http://antares.in2p3.fr/internal/dokuwiki/doku.php?id=distlines

L'avant propos de Richard Monvoisin m'inspire et me donne envie de faire le mien, je pourrais même citer une de ses dédicaces que je trouve superbe:
"Je dédie ce texte aux cousins non-blancs, qui, par ce qui a été spôlié à leurs ancêtres, m’ont indirectement permis de faire les études qu’eux ne feront probablement pas, à moins de se glisser dans un train d’atterrissage d’un avion de la Sabena."
Si je souligne le fait que je suis arrivé jusque là en partie parce que je suis né dans un pays riche et que je suis un homme (les maths et la physique c'est des trucs de garçons) je pourrais dire que le seul truc que j'ai loupé c'est de ne pas être né dans un pays anglophone :-)

# À retenir pour la soutenance
neutrino from kilonova (decay) sont plus basses énergy et trop bas flux (ce serait vu par hyperK si c'était à 1Mpc et par IC à 10 Mpc ce qui est assez loin)

Weak Equivalence principle: masse pesante = masse inertielle
Einstein Equivalence principle: Effet d'un champ gravitationel sur une expérience est identique à une accélération du référentiel de l'observateur (comme l'histoire de l'ascenceur qui monte mais avec le même effet sur lumière) ssi ce n'est pas une expérience de gravitation
Equivalence principle Strong: idem mais inclut les éxpériences de gravitation

# Idées de citations
Dans l'idée des citations de début de chapitre je pourrait mettre that "I have found the truth and it is made of pasta"
Neutrino astronomy ou résultats: "L'inexistence de la preuve n'est pas la preuve de l'inexistence"
Probablement pas adapté mais au cas où: "On ne pourra jamais compter toutes les étoiles qui nous entourent. De toute façon ce serait inutile, il suffit d'en compter la moitié et de multiplier par deux." Le Chat
"Les chiffres de la violence sont en hausse …mais qui nous dit que le nombre de non-violents ne l'est pas lui aussi ?" Le Chat
Introduction: "Begin at the beginning," the King said, gravely, "and go on till you come to the end; then stop." Lewis Carroll, *Alice in Wonderland*
Conclusion: 'Tout à une fin, sauf la banane qui en a deux' Proverbe Africain (soit disant)
Results: "Je n'ai jamais dit que j'étais Batman, mais on ne nous a jamais vus dans la même pièce…"
Je ne pense pas vraiment l'utiliser, mais à propos de l'événement multimess d'IC cette citation va bien: "Assurons-nous bien du fait avant de nous inquiéter de la cause" Bernard Le Bovier de Fontenelle
Éventuellement, si je trouve un endroit où le mettre "Mais oui c'est clair" Eddy Malou mais bon, c'est pas trop ma philosophie jusque là
"Reports that say that something hasn't happened are always interesting to me, because as we know, there are known knowns; there are things we know we know. We also know there are known unknowns; that is to say we know there are some things we do not know. But there are also unknown unknowns – the ones we don't know we don't know. And if one looks throughout the history of our country and other free countries, it is the latter category that tend to be the difficult ones." ce qui m'embête c'est que ça a été utilisé pour convaincre qu'on devait taper sur l'Iraq parce que "on sait jamais"
Probablement impossible à mettre quelque part mais on sait jamais: "The only thing in the software field that is worse than an unauthorised copy of a proprietary program, is an authorised copy of the proprietary program because this does the same harm to its whole community of users, and in addition, usually the developer, the perpetrator of this evil, profits from it." Stallman

En attendant que les gens entre dans la salle et s'installe, je pourrais mettre cette image: https://ixquick-proxy.com/do/show_picture.pl?l=francais&rais=1&oiu=http%3A%2F%2F1.bp.blogspot.com%2F-Us_WLo8aLGs%2FVCq_566jfkI%2FAAAAAAAAFPw%2FSrNlijyDakM%2Fs1600%2FA.jpg&sp=f3f86c90ce5ce46f57bfae162efc30f5



# Vulgarisation
Dire que les gw det peuvent déduires pleins d'infos de la forme du signal comme la masse, la distance… (donner autres exemples et dire que les dét optiques ne peuvent pas toujours obtenir ces informations)

http://public.virgo-gw.eu/wp-content/uploads/2017/10/infographic-fr.pdf

Dire que les sujets d'astro sur lesquels j'ai travaillé sont des trucs sur lesquels on a des idées vagues mais peu de certitudes, on est à la masse et nu ou multimess permettrons d'apprendre des choses qu'on aurait pas pu apprendre autrement.

Pour vulgariser à mon entourage avant ma thèse, ou plutôt pour faire comprendre à chacun la limite de ma vulgarisation étant donné que je ne suis pas capable de leur rendre intuitif ce qu'est un neutrino: expliquer ce qu'est un neutrino (ou une particule) a des néophytes c'est comme expliquer à platon ce qu'est un ordinateur, on peut donner des images (c'est une sorte de cerveau pas vivant en métal…) mais tant que platon ne sait pas ce que peut et ne peut pas faire un ordinateur l'image qu'il va s'en faire sera peu précise et peu exacte. Décrire une particule en disant que c'est une bille c'est comme décrire un ordinateur à Platon en disant que c'est un cerveau, ça permet de comprendre certaines choses mais ça peut induire en erreur sur d'autres.
En tant que physiciens on est pas forcément capable de se représenter physiquement à quoi ça ressemble mais on sait comment ça se comporte. Comme si Platon vous avait posé pleins de questions sur ce que peut ou pas faire un ordinateur, il ne se le représenterait pas bien, il ne comprendrait toujours pas son fonctionnement interne mais il serait capable de dire des choses, dans quel cas un ordinateur serait utile ou pas par exemple.
(je pense que ce qui fait qu'on est perdu quand on nous parle de particules c'est qu'on se représente une bille en ne sachant pas bien à quel point on est loin de la réalité, finalement c'est pas si loin)

À partir de la taille de l'étoile et de google map on pourrait faire une image

~1.4 Masses solaires sur ~14km radius (utiliser des masse terrestres plutôt)

Neil de Grass Tyson: Si le marteau de Thor était fait de la matière d'une étoile à neutron, il serait aussi lourd que 300 milliards d'élephants

# Astro multimessager
Voir Wikipedia (et lire papiers pour confirmer ainsi que bouquin de Maurizio « Particles and astrophysics a multimessenger approach »)
Premiers truc multimess seraient:
1940s: Some cosmic rays are identified as forming in solar flares.[7]
1987: Supernova SN 1987A, which was first detected with an optical telescope, also emitted neutrinos that were detected at the Kamiokande-II, IMB and Baksan neutrino observatories.
Puis GW170817
(Wikipedia cite aussi l'evt IC, ce serait bien que j'étudie la question)

# Présentation:
Utiliser des bibliothèques gratuites d'images libre de droit si besoin
Cette image peut être cool à utiliser (même dans ma thèse) https://upload.wikimedia.org/wikipedia/commons/5/57/FirstNeutrinoEventAnnotated.jpg

# Outline physique GW:
- General relativity and GW (rapidement)
- advance LIGO, advance Virgo detectors (rapidement)
- GW170817 event
	- Chronology of the detection
	- Résultats de cette observation
	% Wikipedia dit: "Scientific interest in the event was enormous, with dozens of preliminary papers (and almost 100 preprints[32]) published the day of the announcement, including eight letters in Science,[13] six in Nature, and 23 in a special issue of The Astrophysical Journal Letters devoted to the subject."
		- Multimessenger follow-up (très rapidement)
		- Gamma ray from NS merger (très rapidement)
		- Test of Hubble constant (très rapidement)
        - Creation of heavy elements
        
		- ...
- Modèles d'émissions de neutrinos par des NS mergers
	- NS mergers
		Pour que ce soit marrant donner les ordres de grandeurs de vitesse de rotation et de taille/masse pour se rendre compte à quel point c'est un truc de fou
	- Short time-scale emission
		- Prompt emission
		- Extended emission
	- Long time-scale emission

# Lik ratio
Éventuellement faire une annexe sur les maths dérrière le rapport de vraissemblance
En particulier parce que je n'ai pas utilisé le -2*llr parce que je ne comprenais pas à l'époque pourquoi, et maintenant je veux montrer que j'ai compris.

# ANTARES detector
Describe background: in particular nu are mainly numu and there is no nutau (so double bang could be cool, see robert bormuth thesis)

Describe the data quality to reference it latter (cherche dans d'autres thèses etc).
Here is something I wrote in my run selection that I can move here:
	The data quality parameter has a value of 0 assigned to runs that have a malfunction of the detector, a high background light due to bioluminescence or for too short runs.  All other runs have a good quality parameter of 1 or even more if the background light is particularly low with a high number of working optical modules.
Donc décrire aussi sparking runs, à quoi ils sont dus et comment on les reconnait (voir ce que j'ai mis dans le run selection, si je l'enlève etc)
Décrit les SCAN runs (cherche dans d'autres thèses etc)(voir ce que j'ai mis dans le run selection, (ANTOINE trouvait ça pas clair))

Décrire le background atmosphérique nu et mu et citer ça dans la section{Weighting of the Monte Carlo Production}

Décrire les triggers, tout ce sur quoi on coupe (reprendre ce qui est dans la section event selection). On décrira les trucs liés aux reco en décrivant les recos.

Décrire EdEdX et Etantra

# Neutrino astronomy
Parler de l'IceCube spectral anomaly (ou dans la partie sur KRAγ ?)



At some point I should develop the supernova thing of the conclusion of the result chapter of the galplane part
